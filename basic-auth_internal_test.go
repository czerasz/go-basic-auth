package basicauth

import (
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestNew(t *testing.T) {
	_, err := New()
	if err != nil {
		t.Errorf("unexpected error")
		return
	}

	_, err = New([]ConfigSetting{errSetting}...)
	if err == nil {
		t.Errorf("expected error")
		return
	}
}

func TestUnauthorized(t *testing.T) {
	rr := httptest.NewRecorder()
	unauthorized(rr, "testauth")

	if rr.Code != http.StatusUnauthorized {
		t.Errorf("expected status code: %d, got: %d", http.StatusUnauthorized, rr.Code)
		return
	}

	res := rr.Result()
	defer res.Body.Close()

	if res.Header.Get("Www-Authenticate") != `Basic realm="testauth"` {
		t.Errorf("expected realm value: %s, got: %s", `Basic realm="testauth"`, res.Header.Get("Www-Authenticate"))
	}
}
