package basicauth

import (
	"errors"
	"os"
	"testing"
)

var errMock = errors.New("mock error")

var errSetting ConfigSetting = func(c *config) error {
	return errMock
}

const wantedPass = "password"

func TestWithUsersFromEnv(t *testing.T) {
	os.Setenv("BASIC_AUTH_USERS", `{"user1":"password"}`)
	defer os.Unsetenv("BASIC_AUTH_USERS")

	c := config{}

	err := WithUsersFromEnv("BASIC_AUTH_USERS", "")(&c)
	if err != nil {
		t.Errorf("unexpected error")
		return
	}

	pass, ok := c.users["user1"]
	if !ok {
		t.Errorf(`missing user "user1"`)
		return
	}

	if pass != wantedPass {
		t.Errorf(`wrong password, wanted: %s, got: %s`, wantedPass, pass)
	}
}

func TestWithUsersFromEnvWithFallback(t *testing.T) {
	c := config{}

	err := WithUsersFromEnv("BASIC_AUTH_USERS", `{"user2":"password"}`)(&c)
	if err != nil {
		t.Errorf("unexpected error")
		return
	}

	pass, ok := c.users["user2"]
	if !ok {
		t.Errorf(`missing user "user2"`)
		return
	}

	if pass != wantedPass {
		t.Errorf(`wrong password, wanted: %s, got: %s`, wantedPass, pass)
	}
}

func TestWithUsersFromEnvWithUnusedFallback(t *testing.T) {
	os.Setenv("BASIC_AUTH_USERS", `{"user3":"password"}`)
	defer os.Unsetenv("BASIC_AUTH_USERS")

	c := config{}

	err := WithUsersFromEnv("BASIC_AUTH_USERS", `{"user2":"password"}`)(&c)
	if err != nil {
		t.Errorf("unexpected error")
		return
	}

	pass, ok := c.users["user3"]
	if !ok {
		t.Errorf(`missing user "user3"`)
		return
	}

	if pass != wantedPass {
		t.Errorf(`wrong password, wanted: %s, got: %s`, wantedPass, pass)
	}
}

func TestWithUsersFromEnvErr(t *testing.T) {
	c := config{}

	err := WithUsersFromEnv("BASIC_AUTH_USERS", "")(&c)
	if err == nil {
		t.Errorf("expected error")
		return
	}

	err = WithUsersFromEnv("BASIC_AUTH_USERS", "{wrong json}")(&c)
	if err == nil {
		t.Errorf("expected error")
	}
}

func TestWithAdditionalUser(t *testing.T) {
	c := config{}

	err := WithAdditionalUser("user4", "password")(&c)
	if err != nil {
		t.Errorf("unexpected error")
		return
	}

	pass, ok := c.users["user4"]
	if !ok {
		t.Errorf(`missing user "user4"`)
		return
	}

	if pass != wantedPass {
		t.Errorf(`wrong password, wanted: %s, got: %s`, wantedPass, pass)
	}
}

func TestWithRealm(t *testing.T) {
	c := config{}

	err := WithRealm("test")(&c)
	if err != nil {
		t.Errorf("unexpected error")
		return
	}

	if c.realm != "test" {
		t.Errorf("wrong realm value, wanted: %s, got: %s", "test", c.realm)
	}
}

func TestApplySettings(t *testing.T) {
	settings := []ConfigSetting{
		WithUsersFromEnv("BASIC_AUTH_USERS", `{"user1": "password 1"}`),
		WithAdditionalUser("user2", "password 2"),
	}

	c := config{}
	err := c.applySettings(settings...)

	if err != nil {
		t.Errorf("unexpected error")
		return
	}

	pass, ok := c.users["user1"]
	if !ok {
		t.Errorf(`missing user "user1"`)
		return
	}

	if pass != "password 1" {
		t.Errorf(`wrong password, wanted: %s, got: %s`, "password 1", pass)
		return
	}

	pass, ok = c.users["user2"]
	if !ok {
		t.Errorf(`missing user "user2"`)
		return
	}

	if pass != "password 2" {
		t.Errorf(`wrong password, wanted: %s, got: %s`, "password 2", pass)
	}
}

func TestApplySettingsErr(t *testing.T) {
	settings := []ConfigSetting{
		errSetting,
	}

	c := config{}
	err := c.applySettings(settings...)

	if err == nil {
		t.Errorf("expected error")
		return
	}
}
