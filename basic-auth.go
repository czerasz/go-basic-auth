package basicauth

import (
	"fmt"
	"net/http"
)

// HandlerFunc is an adapter to allow the use of ordinary functions as Negroni handlers.
// If f is a function with the appropriate signature, HandlerFunc(f) is a Handler object that calls f.
type HandlerFunc func(rw http.ResponseWriter, r *http.Request, next http.HandlerFunc)

// ServeHTTP implements the negroni.Handler interface
func (h HandlerFunc) ServeHTTP(rw http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
	h(rw, r, next)
}

// BasicAuth is the basic auth interface
type BasicAuth interface {
	Auth() HandlerFunc
}

// New creates a new basic auth configuration
func New(settings ...ConfigSetting) (BasicAuth, error) {
	c := config{
		realm: "Authorization Required",
	}
	if err := c.applySettings(settings...); err != nil {
		return nil, err
	}

	return &c, nil
}

// Auth implements the BasicAuth interface
func (c *config) Auth() HandlerFunc {
	return basicFunc(c.realm, func(user, pass string, req *http.Request) bool {
		if userpass, ok := c.users[user]; ok {
			if pass == userpass {
				return true
			}
		}

		return false
	})
}

func unauthorized(w http.ResponseWriter, realm string) {
	w.Header().Set("WWW-Authenticate", fmt.Sprintf(`Basic realm="%s"`, realm))
	http.Error(w, "Not Authorized", http.StatusUnauthorized)
}

type authFunc func(string, string, *http.Request) bool

func basicFunc(realm string, authfn authFunc) HandlerFunc {
	return func(res http.ResponseWriter, req *http.Request, next http.HandlerFunc) {
		user, pass, authOK := req.BasicAuth()

		if !authOK || !authfn(user, pass, req) {
			unauthorized(res, realm)
			return
		}

		next(res, req)
	}
}
