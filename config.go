package basicauth

import (
	"encoding/json"
	"errors"
	"os"
)

type config struct {
	users map[string]string
	realm string
}

// ConfigSetting is the type used for predefined settings "creators"
type ConfigSetting func(*config) error

func (c *config) applySettings(settings ...ConfigSetting) error {
	for _, setting := range settings {
		if err := setting(c); err != nil {
			return err
		}
	}

	return nil
}

var ErrNoEnvVariable = errors.New("environment variable not set and no fallback value provided")

// WithUsersFromEnv reads the basic auth users from environment variable
func WithUsersFromEnv(envVarName, fallback string) ConfigSetting {
	return func(c *config) error {
		rawUsers, ok := os.LookupEnv(envVarName)
		if !ok {
			if fallback == "" {
				return ErrNoEnvVariable
			}

			rawUsers = fallback
		}

		err := json.Unmarshal([]byte(rawUsers), &c.users)
		if err != nil {
			return err
		}

		return nil
	}
}

// WithAdditionalUser appends a new basic auth user
func WithAdditionalUser(username, password string) ConfigSetting {
	return func(c *config) error {
		if c.users == nil {
			c.users = map[string]string{}
		}

		c.users[username] = password

		return nil
	}
}

// WithRealm configures a realm
func WithRealm(realm string) ConfigSetting {
	return func(c *config) error {
		c.realm = realm

		return nil
	}
}
