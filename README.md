# Golang Basic Auth Middleware

## Usage

```go
var router *mux.Router = mux.NewRouter().StrictSlash(true)

// Initialize BasicAuth
settings := []basicauth.ConfigSetting{
  basicauth.WithUsersFromEnv("BASIC_AUTH_USERS", `{"local":"password"}`),
  basicauth.WithAdditionalUser("test", "test password"),
}

basicAuth, err := basicauth.New(settings...)
if err != nil {
  log.Fatalf("error during basic auth initialization: %s", err)
}

router.PathPrefix("/api").Handler(negroni.New(
  negroni.HandlerFunc(basicAuth.Auth()),
  negroni.Wrap(...), // <- Your router goes here
))

n := negroni.Classic() // include default middlewares
n.UseHandler(router)

srv := &http.Server{
  Handler: n,
  Addr:    "0.0.0.0:8080",
  // Good practice: enforce timeouts for servers you create!
  WriteTimeout: 2 * time.Second,
  ReadTimeout:  2 * time.Second,
}
log.Print("Server listening on http://0.0.0.0:8080/")
log.Fatal(srv.ListenAndServe())
```