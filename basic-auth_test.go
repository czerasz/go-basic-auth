package basicauth_test

import (
	"net/http"
	"net/http/httptest"
	"testing"

	basicauth "gitlab.com/czerasz/go-basic-auth"
)

func TestAuth(t *testing.T) {
	s := []basicauth.ConfigSetting{
		basicauth.WithAdditionalUser("user1", "password 1"),
	}
	m, err := basicauth.New(s...)

	if err != nil {
		t.Errorf("unexpected error")
		return
	}

	req := httptest.NewRequest(http.MethodGet, "/some/path", nil)
	req.SetBasicAuth("user1", "password 1")

	rr := httptest.NewRecorder()
	m.Auth().ServeHTTP(rr, req, func(w http.ResponseWriter, r *http.Request) {})

	if rr.Code != http.StatusOK {
		t.Errorf("wrong status code, wanted: %d, got: %d", http.StatusOK, rr.Code)
	}
}

func TestAuthErr(t *testing.T) {
	s := []basicauth.ConfigSetting{
		basicauth.WithAdditionalUser("user1", "password 1"),
	}
	m, err := basicauth.New(s...)

	if err != nil {
		t.Errorf("unexpected error")
		return
	}

	req := httptest.NewRequest(http.MethodGet, "/some/path", nil)
	req.SetBasicAuth("user2", "password 2")

	rr := httptest.NewRecorder()
	m.Auth().ServeHTTP(rr, req, func(w http.ResponseWriter, r *http.Request) {
		t.Errorf("next should not be called")
	})

	if rr.Code != http.StatusUnauthorized {
		t.Errorf("wrong status code, wanted: %d, got: %d", http.StatusUnauthorized, rr.Code)
	}
}
