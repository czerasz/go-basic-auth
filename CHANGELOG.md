## [0.1.1](https://gitlab.com/czerasz/go-basic-auth/compare/v0.1.0...v0.1.1) (2020-07-27)


### Bug Fixes

* CI test ([70e526e](https://gitlab.com/czerasz/go-basic-auth/commit/70e526eeb76cb425c28624302cb286e6d31dfeab))


### Continuous Integrations

* add CI configuration ([012d56c](https://gitlab.com/czerasz/go-basic-auth/commit/012d56c39ae7e4af9f36854cabedc2cad26d958e))


### Documentation

* add documentation ([fc1bcfb](https://gitlab.com/czerasz/go-basic-auth/commit/fc1bcfb1e0c9b06010779b06630250cedd52c2ae))
